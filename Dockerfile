FROM registry.gitlab.com/dedyms/node:14-dev AS tukang
USER $CONTAINERUSER
WORKDIR $HOME
RUN git clone --depth=1 https://github.com/IrosTheBeggar/mStream.git mstream && rm -rf /home/$CONTAINERUSER/mstream/.git /home/$CONTAINERUSER/mstream/docs
WORKDIR /home/$CONTAINERUSER/mstream
RUN npm install --prod


FROM registry.gitlab.com/dedyms/node:14
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER /home/$CONTAINERUSER/mstream/ /home/$CONTAINERUSER/mstream/
WORKDIR /home/$CONTAINERUSER/mstream
USER $CONTAINERUSER
VOLUME /home/$CONTAINERUSER/mstream/save
VOLUME /home/$CONTAINERUSER/mstream/image-cache
CMD ["node", "cli-boot-wrapper.js", "-j", "save/conf/config.json"]
